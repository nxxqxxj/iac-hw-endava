output "public_ip_address" {
  value = azurerm_linux_virtual_machine.IaC-HW-VM.public_ip_address
}

output "tls_private_key" {
  value     = tls_private_key.IaC-HW-VM-SSHKey.private_key_pem
  sensitive = true
}