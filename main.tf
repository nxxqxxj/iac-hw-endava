##########  PROVIDER DECLARATION   ##########
terraform {
required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

##########  CREATING RGs   ##########
resource "azurerm_resource_group" "IaC-HW-EUS1-RG" {
  name     = "IaC-HW-EUS1-RG"
  location = "East US"
}

resource "azurerm_resource_group" "IaC-HW-EUS2-RG" {
  name     = "IaC-HW-EUS2-RG"
  location = "East US 2"
}

##########  CREATING VNETs   ##########
resource "azurerm_virtual_network" "IaC-HW-EUS1-VNet" {
  name                = "IaC-HW-EUS1-VNet"
  location            = azurerm_resource_group.IaC-HW-EUS1-RG.location
  resource_group_name = azurerm_resource_group.IaC-HW-EUS1-RG.name
  address_space       = ["10.10.0.0/16"]
}

resource "azurerm_subnet" "EUS1-VNet-SNet" {
  name                 = "EUS1-VNet-SNet"
  resource_group_name  = azurerm_resource_group.IaC-HW-EUS1-RG.name
  virtual_network_name = azurerm_virtual_network.IaC-HW-EUS1-VNet.name
  address_prefixes     = ["10.10.1.0/24"]
}

resource "azurerm_virtual_network" "IaC-HW-EUS2-VNet" {
  name                = "IaC-HW-EUS2-VNet"
  location            = azurerm_resource_group.IaC-HW-EUS2-RG.location
  resource_group_name = azurerm_resource_group.IaC-HW-EUS2-RG.name
  address_space       = ["10.20.0.0/16"]
}

resource "azurerm_subnet" "EUS2-VNet-SNet" {
  name                 = "EUS2-VNet-SNet"
  resource_group_name  = azurerm_resource_group.IaC-HW-EUS2-RG.name
  virtual_network_name = azurerm_virtual_network.IaC-HW-EUS2-VNet.name
  address_prefixes     = ["10.20.1.0/24"]
}

##########  PEERING VNETs   ##########
resource "azurerm_virtual_network_peering" "IaC-HW-EUS1-Peer1to2" {
  name                      = "IaC-HW-EUS1-Peer1to2"
  resource_group_name       = azurerm_resource_group.IaC-HW-EUS1-RG.name
  virtual_network_name      = azurerm_virtual_network.IaC-HW-EUS1-VNet.name
  remote_virtual_network_id = azurerm_virtual_network.IaC-HW-EUS2-VNet.id
}

resource "azurerm_virtual_network_peering" "IaC-HW-EUS2-Peer2to1" {
  name                      = "IaC-HW-EUS2-Peer2to1"
  resource_group_name       = azurerm_resource_group.IaC-HW-EUS2-RG.name
  virtual_network_name      = azurerm_virtual_network.IaC-HW-EUS2-VNet.name
  remote_virtual_network_id = azurerm_virtual_network.IaC-HW-EUS1-VNet.id
}

##########  SETTING UP THE VM   ##########
resource "azurerm_public_ip" "IaC-HW-VM-pIP" {
  name                = "IaC-HW-VM-pIP"
  resource_group_name = azurerm_resource_group.IaC-HW-EUS1-RG.name
  location            = azurerm_resource_group.IaC-HW-EUS1-RG.location
  allocation_method   = "Static"
}

resource "azurerm_network_security_group" "IaC-HW-VM-NSG" {
  name                = "IaC-HW-VM-NSG"
  resource_group_name = azurerm_resource_group.IaC-HW-EUS1-RG.name
  location            = azurerm_resource_group.IaC-HW-EUS1-RG.location

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTP"
    priority                   = 1011
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface" "IaC-HW-VM-NIC" {
  name                = "IaC-HW-VM-NIC"
  resource_group_name = azurerm_resource_group.IaC-HW-EUS1-RG.name
  location            = azurerm_resource_group.IaC-HW-EUS1-RG.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.EUS1-VNet-SNet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.IaC-HW-VM-pIP.id
  }
}

resource "azurerm_network_interface_security_group_association" "NGS-to-NIC" {
  network_interface_id      = azurerm_network_interface.IaC-HW-VM-NIC.id
  network_security_group_id = azurerm_network_security_group.IaC-HW-VM-NSG.id
}

resource "tls_private_key" "IaC-HW-VM-SSHKey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "azurerm_linux_virtual_machine" "IaC-HW-VM" {
  name                  = "IaC-HW-VM"
  resource_group_name   = azurerm_resource_group.IaC-HW-EUS1-RG.name
  location              = azurerm_resource_group.IaC-HW-EUS1-RG.location
  network_interface_ids = [azurerm_network_interface.IaC-HW-VM-NIC.id]
  size                  = "Standard_B2s"

  os_disk {
    name                 = "osDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  computer_name                   = "IaC-HW-VM"
  admin_username                  = "nxxqxxj"
  disable_password_authentication = true
  custom_data                     = filebase64("data.tpl")

  admin_ssh_key {
    username   = "nxxqxxj"
    public_key = tls_private_key.IaC-HW-VM-SSHKey.public_key_openssh
  }
}